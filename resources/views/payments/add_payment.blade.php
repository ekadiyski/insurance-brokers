@extends('layouts.startmin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Полици</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-md-11">
            <div class="panel panel-default">
                <div class="panel-heading"> <i class="fa fa-line-chart" aria-hidden="true"></i> Добави ново плащане по полица </div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    @include('partials.errors')
                    
                    <div>
                        Отминали плащания:
                        <?php $num = 1; ?>
                        @foreach($payments_history as $payment)
                            <p> {{$num}}) Сума: {{$payment->amount}}, Зелена карта: {{$payment->green_card}}, Стикер: {{$payment->sticker}}, Дата на плащане: {{$payment->date_paid}}  </p>
                        <?php $num++; ?>
                        @endforeach
                    </div>

                    <div>
                        Оставащи плащания:
                        <?php $num = 1; ?>
                        @foreach($left_payments as $payment)
                            <p> 
                                {{$num}}) Сума: {{$payment->amount}}  
                                @if($num == 1)
                                    - текущо плащане
                                    <?php 
                                        $current_payment = $payment->amount;
                                        $payment_due_date = Carbon\Carbon::parse($payment->payment_due_date)->format('Y-m-d');
                                    ?>
                                @endif
                            </p>
                        <?php $num++; ?>
                        @endforeach
                    </div>

                    <form method="post" action="{{url('/payments/store')}}" class="form-horizontal" id="inputForm">
                        {{csrf_field()}}
                        <?php $user_id = Auth::id(); ?>
                        <input type="hidden" name="updated_by" value="{{$user_id}}">
                        <input type="hidden" name="broker_agent_id" value="1">
                        <input type="hidden" name="current_payment" value="{{$current_payment}}">
                        <input type="hidden" name="payment_id" value="{{$first_unpaid->id}}">

                        <div class="panel panel-default">
                          <div class="panel-heading">Плащане</div>
                          <div class="panel-body">
                            <div class="row">
                                <div class="input-group">
                                    <input type="text" class="form-control col-md-5" id="current_payment" name="current_payment" value="{{$current_payment}} лв." disabled>
                                    <span class="input-group-addon">-</span>

                                    <input type="text" class="form-control col-md-5" id="payment_due_date" name="payment_due_date" value="{{$payment_due_date}}" disabled>
                                    <span class="input-group-addon">-</span>                                    
                                </div>
                            </div>

                            <p> </p>

                            <div class="row">
                                <div class="input-group">
                                    <div class="input-group date" id="datetimepicker1">
                                        <input class="form-control" type="text" id="date_paid" name="date_paid" value="{{old('date_paid')}}" placeholder="Дата на плащане*" required>
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                        </span>
                                    </div>
                                    <span class="input-group-addon">-</span>

                                    @if($cm_contract->cm_vehicle_id)
                                        <input type="text" class="form-control col-md-5" id="green_card" name="green_card" placeholder="Зелена карта*" value="{{ old('green_card') }}">
                                        <span class="input-group-addon">-</span>

                                        <input type="text" class="form-control col-md-5" id="sticker" name="sticker" placeholder="Стикер*" value="{{ old('sticker') }}">
                                    @endif
                                </div>
                            </div>
                          </div>
                        </div>


                        <div class="row">
                            <button type="submit" class="btn btn-primary pull-right" style="margin:0 15px;">Добави</button>
                            <a href="{{url('/contracts')}}" class="btn btn-default pull-right"> Отказ </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('footer_scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.full.min.js"></script>

<link href="{{asset('vendor/datetimepicker/jquery.datetimepicker.css')}}" rel="stylesheet">

<script type="text/javascript">
    $(document).ready(function() {
        $('#datetimepicker1').find('input').datetimepicker({
            format: 'Y-m-d',
            timepicker: false,
        });
    })
</script>
@endsection