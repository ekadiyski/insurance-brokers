@extends('layouts.startmin')

@section('content')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Списък с предстоящи вноски</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Информация за предстоящи вноски по полици
                        </div>
                        @include('partials.success_msg')
                        <p>
                              <!-- add Добави плащане btn with form input politsa number/egn_or_eik/customer names -->  
                        </p>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover text-center" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Дата</th>
                                        <th>Клиент</th>

                                        <th>Продукт</th>
                                        <th>Обща сума</th>
                                        <th>Дата на падеж</th>
                                        
                                        <th>№ на полица</th>

                                        
                                        <th>Телефон</th>
                                        
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $num = 1; ?>
                                    @foreach($payments as $payment)
                                        <tr>
                                            <td>{{$num++}}</td>
                                            
                                            <td>{{Carbon\Carbon::parse($payment->date_paid)->format('Y-m-d')}}</td>
                                            
                                            <td>
                                                @if($payment->cm_contract && $payment->cm_contract->cm_customer)
                                                    {{$payment->cm_contract->cm_customer->first_name}} {{$payment->cm_contract->cm_customer->last_name}}
                                                @endif
                                            </td>

                                            <td>{{$payment->cm_contract->cl_product_id}}</td>
                                            
                                            
                                            <td>{{$payment->amount}}</td>
                                            <td> {{Carbon\Carbon::parse($payment->payment_due_date)->format('Y-m-d')}}</td>
                                            
                                            <td>{{$payment->cm_contract->politsa_number}}</td>

                                            <td>
                                                @if($payment->cm_contract->cm_vehicle) 
                                                    {{$payment->cm_contract->cm_vehicle->cm_customer->phone}} 
                                                @endif
                                            </td>
                                            

                                            <td>
                                                <?php $cm_contract_id = $payment->cm_contract->id; ?>
                                                @if($payment->cm_contract->total_price > $payment->cm_contract->already_paid)
                                                    <a href='{{url("/payments/add/$cm_contract_id")}}' class="btn btn-warning text-center" title="Добави плащане"> Плащане </a>
                                                @else
                                                    Изплатена
                                                @endif
                                            </td>

                                            <td>
                                                <a href='{{url("/contracts/$cm_contract_id/edit")}}' class="btn btn-success text-center" title="Промени" disabled><i class="fa fa-edit"></i> </a>
                                            </td>
                                            <td>
                                                <form action='{{url("/contracts/$cm_contract_id")}}' method="POST" onsubmit="return confirm('Наистина ли искате да изтриета тази полица?');">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}

                                                    <button type="submit" class="btn btn-danger text-center" title="Изтрий" id="delete" disabled>
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>

            
@endsection

@section('footer_scripts')
<script src="{{asset('vendor/datatables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendor/datatables-plugins/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('vendor/datatables-responsive/dataTables.responsive.js')}}"></script>

<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            "iDisplayLength": 25,
            responsive: true,
        });
    });
</script>


@endsection
