@extends('layouts.startmin')

@section('content')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Списък с вноски по полици</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Информация за вноски по полици
                        </div>
                        @include('partials.success_msg')
                        <p>
                              <!-- add Добави плащане btn with form input politsa number/egn_or_eik/customer names -->  
                        </p>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover text-center" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Седмица</th>
                                        <th>Дата</th>
                                        <th>Консултант</th>
                                        <th>Клиент</th>

                                        <th>Продукт</th>
                                        <th>Застр. компания</th>
                                        <th>Вноски</th>
                                        <th>сума без 2% и без 11.5 лв.</th>
                                        <th>премия без стикер</th>
                                        <th>Обща сума</th>
                                        <th>Дата на падеж</th>
                                        <th>Забележка</th>
                                        <th>№ на полица</th>

                                        <th>Зелена карта</th>
                                        <th>Стикер</th>
                                        
                                        <th>Номер на талон</th>
                                        <th>Марка на МПС</th>
                                        <th>Телефон</th>
                                        <th>ЕГН на клиента</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $num = 1; ?>
                                    @foreach($payments as $payment)
                                        <tr>
                                            <td>{{$num++}}</td>
                                            <td>[{{Carbon\Carbon::parse($payment->date_paid)->format('W')}}]</td>
                                            <td>{{Carbon\Carbon::parse($payment->date_paid)->format('Y-m-d')}}</td>
                                            <td> {{$payment->user_created->name}}</td>
                                            <td>
                                                @if($payment->cm_contract && $payment->cm_contract->cm_customer)
                                                    {{$payment->cm_contract->cm_customer->first_name}} {{$payment->cm_contract->cm_customer->last_name}}
                                                @endif
                                            </td>

                                            <td>{{$payment->cm_contract->cl_product_id}}</td>
                                            <td>{{$payment->cm_contract->cl_insurance_company_id}}</td>
                                            <td>{{$payment->sequence}} от {{$payment->cm_contract->payments_count}}</td>
                                            
                                            <td>{{$payment->amount_without2}}</td>
                                            <td>{{$payment->amount_no_sticker}}</td>
                                            <td>{{$payment->amount}}</td>
                                            <td> {{Carbon\Carbon::parse($payment->next_payment_due_date)->format('Y-m-d')}}</td>
                                            <td> 
                                                @if($payment->cm_contract->cm_vehicle)
                                                    {{$payment->cm_contract->cm_vehicle->name}} 
                                                @endif 
                                            </td>
                                            <td>{{$payment->cm_contract->politsa_number}}</td>
                                            
                                            <td> {{$payment->green_card}} </td>                                 
                                            <td> {{$payment->sticker}} </td>

                                            <td> @if($payment->cm_contract->cm_vehicle) 
                                                    {{$payment->cm_contract->cm_vehicle->talon_number}} 
                                                @endif
                                            </td>
                                            <td> @if($payment->cm_contract->cm_vehicle) 
                                                    {{$payment->cm_contract->cm_vehicle->brand}} 
                                                @endif
                                            </td>
                                            <td> @if($payment->cm_contract && $payment->cm_contract->cm_customer) 
                                                    {{$payment->cm_contract->cm_customer->phone}} 
                                                @endif
                                            </td>
                                            <td> @if($payment->cm_contract && $payment->cm_contract->cm_customer) 
                                                    {{$payment->cm_contract->cm_customer->egn_eik}} 
                                                @endif
                                            </td>

                                            <?php $cm_contract_id = $payment->cm_contract->id; ?>
                                            <td>
                                                <a href='{{url("/contracts/$cm_contract_id/edit")}}' class="btn btn-success text-center" title="Промени" disabled><i class="fa fa-edit"></i> </a>
                                            </td>
                                            <td>
                                                <form action='{{url("/contracts/$cm_contract_id")}}' method="POST" onsubmit="return confirm('Наистина ли искате да изтриета тази полица?');">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}

                                                    <button type="submit" class="btn btn-danger text-center" title="Изтрий" id="delete" disabled>
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>

            
@endsection

@section('footer_scripts')
<script src="{{asset('vendor/datatables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendor/datatables-plugins/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('vendor/datatables-responsive/dataTables.responsive.js')}}"></script>

<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>

<script>
    $(document).ready(function() {
        //var current_date = new Date().toJSON().slice(0,10).replace(/-/g,'/');
        var current_date = new Date().toISOString().substring(0, 10)
        var file_name = 'Застрахователни-полици (' + current_date + ')';
        $('#dataTables-example').DataTable({
            "iDisplayLength": 25,
            responsive: true,

            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: 'Свали в Excel',
                    exportOptions: {
                        columns: [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
                    },
                    title: file_name
                }
                //,'colvis'
            ]
        });
    });
</script>


@endsection
