@extends('layouts.startmin')

@section('content')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Списък с агенти</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Информация за агенти
                        </div>
                        @include('partials.success_msg')
                        <p>
                              <!-- add Добави плащане btn with form input politsa number/egn_or_eik/customer names -->  
                        </p>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover text-center" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Име</th>
                                        <th>Ел. поща</th>
                                        <th>Роля</th>

                                        <th>Брой договори</th>
                                        <th>Брой плащания</th>                                       
                                        <th>Дата на присъединяване</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $num = 1; ?>
                                    @foreach($agency_users as $user)
                                        <tr>
                                            <td>{{$num++}}</td>
                                            <td> {{$user->name}} </td>
                                            <td> {{$user->email}} </td>
                                            <td> {{$user->role}} </td>

                                            <td> {{$user->contracts_count}} </td>
                                            <td> {{$user->payments_count}} </td>
                                            <td> {{Carbon\Carbon::parse($user->created_at)->format('Y-m-d')}}</td>
                                            
                                            <?php $user_id = $user->id; ?>
                                            <td>
                                                <a href='{{url("/users/contracts/$user_id")}}' class="btn btn-info text-center" title="Полици" disabled><i class="fa fa-file-text-o  "></i> Полици </a>
                                            </td>
                                            <td>
                                                <a href='{{url("/users/payments/$user_id")}}' class="btn btn-info text-center" title="Плащания" disabled><i class="fa fa-eur"></i> Плащания </a>
                                            </td>
                                            <td>
                                                <form action='{{url("/users/$user_id")}}' method="POST" onsubmit="return confirm('Наистина ли искате да изтриета този агент?');">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}

                                                    <button type="submit" class="btn btn-danger text-center" title="Изтрий" id="delete">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>

            
@endsection

@section('footer_scripts')
<script src="{{asset('vendor/datatables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendor/datatables-plugins/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('vendor/datatables-responsive/dataTables.responsive.js')}}"></script>

<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            "iDisplayLength": 25,
            responsive: true,
        });
    });
</script>


@endsection
