@extends('layouts.startmin')

@section('content')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Списък с полици</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Информация за полици
                        </div>
                        @include('partials.success_msg')
                        <p>
                            <div class="row">
                                <div class="col-md-2">
                                    <a href="{{url('contracts/create')}}" class="btn btn-primary"> Добави нова полица </a>
                                </div>
                            </div>
                        </p>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover text-center" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Продукт</th>
                                        <th>Компания</th>
                                        <th>№ на полица</th>
                                        <th>Дата</th>
                                        <th>Брой вноски</th>
                                        <th>Стойност</th>
                                        <th>Изплатени</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $num = 1; ?>
                                    @foreach($cm_contracts as $cm_contract)
                                        <tr>
                                            <td>{{$num++}}</td>
                                            <td>{{$cm_contract->cl_product_id}}</td>
                                            <td>{{$cm_contract->cl_insurance_company_id}}</td>
                                            <td>{{$cm_contract->politsa_number}}</td>
                                            <td>{{Carbon\Carbon::parse($cm_contract->date_signed)->format('Y-m-d')}}</td>

                                            <td>{{$cm_contract->payments_count}}</td>
                                            <td>{{$cm_contract->total_price}}</td>
                                            <td>{{$cm_contract->already_paid}}</td>
                                            
                                            <td>
                                                
                                            </td>

                                            <td>
                                                @if($cm_contract->total_price > $cm_contract->already_paid)
                                                    <a href='{{url("/payments/add/$cm_contract->id")}}' class="btn btn-warning text-center" title="Добави плащане"> Плащане </a>
                                                @else
                                                    Изплатена
                                                @endif
                                            </td>
                                            <td>
                                                <a href='{{url("/contracts/$cm_contract->id/edit")}}' class="btn btn-success text-center" title="Промени" disabled><i class="fa fa-edit"></i> </a>
                                            </td>
                                            <td>
                                                <form action='{{url("/contracts/$cm_contract->id")}}' method="POST" onsubmit="return confirm('Наистина ли искате да изтриета тази полица?');">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}

                                                    <button type="submit" class="btn btn-danger text-center" title="Изтрий" id="delete">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>

            
@endsection

@section('footer_scripts')
<script src="{{asset('vendor/datatables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendor/datatables-plugins/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('vendor/datatables-responsive/dataTables.responsive.js')}}"></script>
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            "iDisplayLength": 25,
            responsive: true
        });
    });
</script>


@endsection
