@extends('layouts.startmin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Полици</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-md-11">
            <div class="panel panel-default">
                <div class="panel-heading"> <i class="fa fa-line-chart" aria-hidden="true"></i> Добави нов договор - {{Auth::user()->name}}</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    @include('partials.errors')

                    <form method="post" action="{{url('/contracts')}}" class="form-horizontal" id="inputForm">
                        {{csrf_field()}}
                        <?php $user_id = Auth::id(); ?>
                        <input type="hidden" name="created_by" value="{{$user_id}}">
                        <input type="hidden" name="broker_agent_id" value="1">
                        

                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <span class="col-md-3"> Клиент </span>
                            <span class="form-check">
                                <?php 
                                    $checked = "";
                                    if(old('use_existing_customer') == 1){
                                        $checked = 'checked';
                                    }
                                ?>
                              <input class="form-check-input" type="checkbox" value="1" name="use_existing_customer" id="use_existing_customer" {{$checked}}>
                              <label class="form-check-label" for="use_existing_customer">
                                Използвай съществуващ 
                              </label>
                            </span>
                          </div>
                          <div class="panel-body">
                            <span id="new-customer">
                                <div class="row">
                                    <div class="input-group">
                                        <input type="text" class="form-control col-md-5" id="first_name" name="first_name" placeholder="Име*" value="{{ old('first_name') }}">
                                        <span class="input-group-addon">-</span>

                                        <input type="text" class="form-control col-md-5" id="middle_name" name="middle_name" placeholder="Презиме" value="{{ old('middle_name') }}">
                                        <span class="input-group-addon">-</span>

                                        <input type="text" class="form-control col-md-5" id="last_name" name="last_name" placeholder="Фамилия" value="{{ old('last_name') }}">
                                        <span class="input-group-addon">-</span>
                                    </div>
                                </div>
                                
                                <p> </p>

                                <div class="row">
                                    <div class="input-group">
                                        <input type="text" class="form-control col-md-5" id="egn_eik" name="egn_eik" placeholder="ЕГН/ЕИК*" value="{{ old('egn_eik') }}">
                                        <span class="input-group-addon">-</span>

                                        <input type="text" class="form-control col-md-5" id="phone" name="phone" placeholder="Телефон" value="{{ old('phone') }}">
                                        <span class="input-group-addon">-</span>

                                        <input type="text" class="form-control col-md-5" id="phone2" name="phone2" placeholder="Телефон2" value="{{ old('phone2') }}">
                                        <span class="input-group-addon">-</span>
                                    </div>
                                </div>
                            </span>

                            <span id="existing-customer" style="display:none;">
                                <div class="col-md-6">
                                    <select name="existing_customer_id" id="existing_customer_id" class="form-control selectpicker" data-live-search="true">
                                        <option value="">--- ЕГН/ЕИК на съществуващ клиент ---</option>
                                        @foreach($agent_customers as $customer)
                                            <?php 
                                                $checked = "";
                                                if(old('existing_customer_id') == $customer->id){
                                                    $checked = 'selected="selected"';
                                                }
                                            ?>
                                            <option value="{{$customer->id}}" data-tokens="{{$customer->egn_eik}}" {{$checked}}> {{$customer->egn_eik}}, {{$customer->first_name}} {{$customer->last_name}} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </span>
                          </div>
                        </div>

                        <div class="panel panel-default">
                          <div class="panel-heading">Полица</div>
                          <div class="panel-body">
                            <div class="row">
                                <div class="input-group">
                                    <select id="cl_product_id" name="cl_product_id" class="form-control col-md-2" required>
                                        <option value="">--- Продукт* ---</option>
                                        @foreach($cl_products as $cl_product)
                                            <?php 
                                                $checked = "";
                                                if(old('cl_product_id') == $cl_product->slug){
                                                    $checked = 'selected="selected"';
                                                }
                                            ?>
                                            <option value="{{$cl_product->slug}}" {{$checked}}> {{$cl_product->name}} ({{$cl_product->slug}}) </option>
                                        @endforeach
                                    </select>

                                    <span class="input-group-addon">-</span>

                                    <select id="cl_insurance_company_id" name="cl_insurance_company_id" class="form-control col-md-3" required>
                                        <option value="">--- Компания* ---</option>
                                        @foreach($cl_insurance_companies as $ins_company)
                                            <?php 
                                                $checked = "";
                                                if(old('cl_insurance_company_id') == $ins_company->name){
                                                    $checked = 'selected="selected"';
                                                }
                                            ?>
                                            <option value="{{$ins_company->name}}" {{$checked}}>{{$ins_company->name}} </option>
                                        @endforeach
                                    </select>

                                    <span class="input-group-addon">-</span>
                                        <span class="form-check">
                                            <?php 
                                                $checked = "";
                                                if(old('no_comission') == '1'){
                                                    $checked = 'checked';
                                                }
                                            ?>
                                            <input class="form-check-input" type="checkbox" value="1" name="no_comission" id="no_comission" {{$checked}}>
                                            <label class="form-check-label" for="no_comission">
                                               Без комисион
                                            </label>
                                        </span>

                                    <span class="input-group-addon">-</span>
                                    
                                    <input type="text" class="form-control col-md-1" id="politsa_number" name="politsa_number" placeholder="№ на полица*" value="{{ old('politsa_number') }}" required>
                                </div>
                            </div>

                            <p> </p>

                            <div class="row">
                                <div class="input-group">
                                    <div class="input-group date" id="datetimepicker1">
                                        <input class="form-control" type="text" id="date_signed" name="date_signed" value="{{old('date_signed')}}" placeholder="Дата на сключване*" required>
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                        </span>
                                    </div>

                                    <span class="input-group-addon">-</span>

                                    <div class="input-group date" id="datetimepicker2">
                                        <input class="form-control" type="text" id="start_date" name="start_date" value="{{old('start_date')}}" placeholder="Начална дата*" required>
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                        </span>
                                    </div>

                                    <span class="input-group-addon">-</span>
                                    <select name="payments_count" class="form-control col-md-1" id="payments_count" required>
                                            <option>--- Изберете брой вноски* ---</option>
                                            <option value="1" <?php echo ((old('payments_count') == 1) ? 'selected' : ''); ?>>1</option>
                                            <option value="2" <?php echo ((old('payments_count') == 2) ? 'selected' : ''); ?>>2</option>
                                            <option value="4" <?php echo ((old('payments_count') == 4) ? 'selected' : ''); ?>>4</option>
                                            <option value="6" <?php echo ((old('payments_count') == 6) ? 'selected' : ''); ?>>6</option>
                                            <option value="12" <?php echo ((old('payments_count') == 12) ? 'selected' : ''); ?>>12</option>
                                    </select>
                                    <span class="input-group-addon"> -</span>

                                    <input type="number" class="form-control col-md-1" id="left_payments_count" name="left_payments_count" placeholder="Оставащ брой вноски*" min="1" max="12" value="{{ old('left_payments_count') }}" required>

                                    <span class="input-group-addon">-</span>

                                    <span class="btn btn-success" id="refresh_payment_list"> Обнови списъка с пащания</span>
                                    
                                </div>
                            </div>

                            <p> </p>
                            <div class="row">
                                <div class="input-group">
                                    <span id="payments_list"> </span>
                                </div>
                            </div>
                          </div>
                        </div>

                        <div class="panel panel-default" id="section-vehicle" style="display:none;">
                          <div class="panel-heading">
                            <span class="col-md-3"> Превозно средство </span>
                            <span class="form-check">
                                <?php
                                    $checked = "";
                                    if(old('use_existing_vehicle') == 'vehicle'){
                                        $checked = 'checked';
                                    }
                                ?>
                              <input class="form-check-input" type="checkbox" value="vehicle" name="use_existing_vehicle" id="use_existing_vehicle" {{$checked}}>
                              <label class="form-check-label" for="use_existing_vehicle">
                                Използвай съществуващо МПС 
                              </label>
                            </span>
                          </div>
                          <div class="panel-body">
                            <span id="new-vehicle">
                                <div class="row">
                                    <div class="input-group">
                                        <input type="text" class="form-control col-md-5" id="name" name="name" placeholder="Регистрационен номер*" value="{{ old('name') }}">
                                        <span class="input-group-addon">-</span>

                                        <input type="text" class="form-control col-md-5" id="brand" name="brand" placeholder="Марка*" value="{{ old('brand') }}">
                                        <span class="input-group-addon">-</span>

                                        <input type="text" class="form-control col-md-5" id="talon_number" name="talon_number" placeholder="№ на талон*" value="{{ old('talon_number') }}">
                                        <span class="input-group-addon">-</span>
                                    </div>
                                </div>
                            </span>

                            <span id="existing-vehicle" style="display:none;">
                                <div class="col-md-6">
                                    <select name="existing_vehicle_id" id="existing_vehicle_id" class="form-control selectpicker2" data-live-search="true">
                                        <option value="">--- Рег. номер на съществуващо МПС ---</option>
                                        @foreach($agent_vehicles as $vehicle)
                                            <?php 
                                                $checked = "";
                                                if(old('existing_vehicle_id') == $vehicle->id){
                                                    $checked = 'selected="selected"';
                                                }
                                            ?>
                                            <option value="{{$vehicle->id}}" data-tokens="{{$vehicle->name}}" {{$checked}}> {{$vehicle->name}}, {{$vehicle->talon_number}} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </span>
                          </div>
                        </div>

                        <div class="row">
                            <button type="submit" class="btn btn-primary pull-right" style="margin:0 15px;">Добави</button>
                            <a href="{{url('/contracts')}}" class="btn btn-default pull-right"> Отказ </a>
                        </div>
                    </form> 
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('footer_scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.full.min.js"></script>

<link href="{{asset('vendor/datetimepicker/jquery.datetimepicker.css')}}" rel="stylesheet">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

<script type="text/javascript">
    var insurances_with_vehicle = ['ГО', 'ЗК', 'ЗЛМ', 'КАСКО', 'Стикер'];
    var insurances_with_sticker = ['ГО', 'ЗК', 'Стикер'];
    var total_amount = 0;

    function is_vehicle_info_visible()
    {
        var selected_product = $('#cl_product_id').val();

        if($.inArray(selected_product, insurances_with_vehicle) > -1){
            $('#section-vehicle').show();
        }else{
            $('#section-vehicle').hide();
        }
    }

    function check_for_existing_customer()
    {
        if($('#use_existing_customer').is(':checked')){
            $('#existing_customer_id').prop('disabled', false);
            $('#first_name').prop('disabled', true);
            $('#egn_eik').prop('disabled', true);

            $('#new-customer').hide();
            $('#existing-customer').show();
        }else{
            $('#existing_customer_id').prop('disabled', true);
            $('#first_name').prop('disabled', false);
            $('#egn_eik').prop('disabled', false);

            $('#new-customer').show();
            $('#existing-customer').hide();
        }
    }

    function check_for_existing_vehicle()
    {
        if($('#use_existing_vehicle').is(':checked')){
            $('#existing_vehicle_id').prop('disabled', false);
            $('#name').prop('disabled', true);

            $('#new-vehicle').hide();
            $('#existing-vehicle').show();
        }else{
            $('#existing_vehicle_id').prop('disabled', true);
            $('#name').prop('disabled', false);

            $('#new-vehicle').show();
            $('#existing-vehicle').hide();
        }
    }

    $(document).ready(function() {
        $('#datetimepicker1').find('input').datetimepicker({
            format: 'Y-m-d',
            timepicker: false,
        });

        $('#datetimepicker2').find('input').datetimepicker({
            format: 'Y-m-d',
            timepicker: false,
        });

        is_vehicle_info_visible();

        if($('#use_existing_customer').is(':checked')){
            check_for_existing_customer();
        }

        $('#use_existing_customer').change(function(){
            check_for_existing_customer();
        });

        if($('#use_existing_vehicle').is(':checked')){
            check_for_existing_vehicle();
        }   

        $('#use_existing_vehicle').change(function(){
            check_for_existing_vehicle();
        });

        $(function() {
            $('.selectpicker').selectpicker();
        });

        $(function() {
            $('.selectpicker2').selectpicker();
        });

        

        $('#cl_product_id').change(function(){
            is_vehicle_info_visible();
        });

        //TODO: on validation error the payments data is lost, even if it was correct!
        $('#refresh_payment_list').click(function() {
            var payments_count = $('#payments_count').val();
            var left_payments_count = $('#left_payments_count').val();
            var start_date = $('#start_date').val();

            if(payments_count <= 0 || payments_count > 6){
                alert('Броят вноски трябва да е между 1 и 6!');
                $('#payments_count').val() = '';
            }else if(payments_count > 1 && (typeof start_date === 'undefined'  || start_date == '')){
                alert('Трябва да изберете начална дата на полицата, за да изчислим следващ(и) падеж(и)!');
            }else{
                total_amount = 0;

                var year = start_date.substring(0,4);
                var month = start_date.substring(5,7);
                var day = start_date.substring(8,10);
                var now = new Date(year, month-1, day);
                
                var months_fee_count = 12 / payments_count;
                var selected_product = $('#cl_product_id').val();

                    var fields = '<div class="row">';
                        //fields += ' ';
                            fields += '<div class="col-md-2"> <input type="number" step="0.01" name="payment[0]" id="payment-0" class="form-control col-md-2" placeholder="Вноска 1" onchange="calcPayments(this, 0, \'' + selected_product + '\')" value="old(payment[0])" required> </div>';
                            fields += '<div class="col-md-2"> <input type="number" step="0.01" name="amount_without2[0]" id="amount_without2-0" placeholder="Сума без 2%"  class="form-control col-md-2" value="old(amount_without2[0])" required readonly> </div>';
                            fields += '<div class="col-md-2"> <input type="number" step="0.01" name="amount_no_sticker[0]" id="amount_no_sticker-0" placeholder="Премия без стикер" class="form-control col-md-2" value="old(amount_no_sticker[0])" required readonly> </div>';
                            
                            var next_payment_date = new Date(now.getFullYear(), now.getMonth() + months_fee_count, now.getDate() + 1);
                            next_payment_date = next_payment_date.toISOString().substring(0, 10);
                            fields += '<input type="hidden" name="next_payment_due_date[0]" id="next_payment_date-0"  value="' + next_payment_date + '" readonly>';

                            if($.inArray(selected_product, insurances_with_sticker) > -1 && payments_count == left_payments_count){
                                fields += '<div class="col-md-3"> <input type="text" class="form-control" name="green_card" placeholder="Зелена карта"> </div>';
                                fields += '<div class="col-md-3"> <input type="text" name="sticker" class="form-control" placeholder="Стикер"> </div>';
                            }
                            
                    fields += '</div> ';
                    fields += '<p> </p>';//just to have some space
                

                //fields += '<div class="row">';
                for(var i = 1; i < left_payments_count; i++){
                    var num = i + 1;
                    var payment_date = new Date(now.getFullYear(), now.getMonth() + i*months_fee_count, now.getDate() + 1);
                    payment_date = payment_date.toISOString().substring(0, 10);

                    var next_payment_date = new Date(now.getFullYear(), now.getMonth() + (i+1)*months_fee_count, now.getDate() + 1);
                    next_payment_date = next_payment_date.toISOString().substring(0, 10);

                    fields += '<div class="row">';
                    fields += '<div class="col-md-3"> <input type="number" step="0.01" name="payment[' + i + ']" id="payment-' + i + '" class="form-control col-md-2" placeholder="Вноска' + num + '" onchange="calcPayments(this,' + i + ', \'' + selected_product + '\')" required> </div>';
                    fields += '<div class="col-md-3"> <input type="number" step="0.01" name="amount_without2[' + i + ']" id="amount_without2-' + i + '" placeholder="Сума без 2%" class="form-control col-md-2" required readonly> </div>';
                    fields += '<div class="col-md-3"> <input type="number" step="0.01" name="amount_no_sticker[' + i + ']" id="amount_no_sticker-' + i + '" placeholder="Премия без стикер" class="form-control col-md-2" required readonly> </div>';
                    fields += '<div class="col-md-3"> <input type="text" name="payment_due_date[' + i + ']" id="payment_date-' + i + '"  value="' + payment_date + '" placeholder="Дата на падеж" class="form-control col-md-2" required readonly> </div>';
                    fields += '<input type="hidden" name="next_payment_due_date[' + i + ']" id="next_payment_date-' + i + '"  value="' + next_payment_date + '" readonly>';
                    fields += '</div> ';
                }
                //fields += '</div>';

                fields += '<p> </p>';
                fields += '<div class="row"> <div class="col-md-4"> Обща сума: <input type="number" step="0.01" name="total_price" id="total_price" placeholder="Обща сума" class="form-control" readonly> </div>';
                $('#payments_list').html(fields);
            }
            
        });
    });

    function calcPayments(curr_amount, index, selected_product){
        var payments_count = $('#payments_count').val();
        var left_payments_count = $('#left_payments_count').val();

        total_amount = 0;
        for(var i = 0; i < left_payments_count; i++){
            var curr_amount_id = '#payment-' + i;

            var amount1 = Number($(curr_amount_id).val());
            total_amount += amount1;
        }
        $('#total_price').val(total_amount.toFixed(2));

        var without2_index = '#amount_without2-' + index;
        
        
        var sticker_tax = 0;
        if(index == 0 && selected_product == 'ГО' && left_payments_count == payments_count){
            sticker_tax = 11.5;
        }

        var no_sticker_index = '#amount_no_sticker-' + index;
        
        var gfo_tax = 1.4;
        if(! $('#no_comission').is(":checked") && selected_product == 'Имущество'){
            gfo_tax = 0;   
        }
        var amount_without2 = (Number(curr_amount.value) - gfo_tax - sticker_tax) / 1.02;

        if($('#no_comission').is(":checked")){
            var amount_no_sticker = 0;
        }else{
            if(selected_product == 'Имущество'){
                var amount_no_sticker = amount_without2;  
            }else{
                var amount_no_sticker = amount_without2 - 1.4;  
            }   
        }
        amount_no_sticker -= sticker_tax;

        amount_without2 = amount_without2.toFixed(2);
        amount_no_sticker = amount_no_sticker.toFixed(2);
        $(without2_index).val(amount_without2);
        $(no_sticker_index).val(amount_no_sticker);
    }
</script>
@endsection
