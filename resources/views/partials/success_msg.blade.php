@if (Session::has('msg_add_success'))
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-info">{{ Session::get('msg_add_success') }}</div>
        </div>
    </div>
@endif

@if (Session::has('msg_update'))
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-success">{{ Session::get('msg_update') }}</div>
        </div>
    </div>
@endif

@if (Session::has('msg_delete'))
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-danger">{{ Session::get('msg_delete') }}</div>
        </div>
    </div>
@endif