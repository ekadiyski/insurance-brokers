<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClInsuranceCompany extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'broker_agent_id', 'slug', 'name',
        'created_at', 'created_by', 'updated_at', 'updated_by', 'deleted_at', 'deleted_by',
    ];
    
    protected $primaryKey = 'name';
    public $incrementing = false;
}
