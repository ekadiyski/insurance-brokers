<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CmCustomer;

use Auth;

class CmVehicle extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'broker_agent_id', 'type', 'name', 'brand', 'talon_number', 'owner_id',
        'created_at', 'created_by', 'updated_at', 'updated_by', 'deleted_at', 'deleted_by',
    ];

    public function cm_customer()
    {
    	return $this->belongsTo('App\Models\CmCustomer', 'owner_id');
    }

    public static function get_agent_vehicles()
    {
        return self::where('created_by', Auth::id());
    }
}
