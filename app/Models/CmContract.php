<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CmCustomer;
use App\Models\CmVehicle;
use App\Models\CmPayment;

use Illuminate\Database\Eloquent\SoftDeletes;

class CmContract extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'broker_agent_id', 'cl_product_id', 'cl_insurance_company_id', 'politsa_number', 'date_signed', 'cm_customer_id', 'cm_vehicle_id',
        'payments_count', 'total_price', 'already_paid', 'created_at', 'created_by', 'updated_at', 'updated_by', 'deleted_by',
    ];

    protected $dates = ['deleted_at'];

    public function cm_vehicle()
    {
    	return $this->belongsTo('App\Models\CmVehicle');
    }

    public function cm_customer()
    {
        return $this->belongsTo('App\Models\CmCustomer');
    }

    public function cm_payments()
    {
    	return $this->hasMany('App\Models\CmPayment');
    }

    public function next_payment_due_date()
    {
        //TODO
    }
}
