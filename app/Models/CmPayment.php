<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CmContract;
use Illuminate\Database\Eloquent\SoftDeletes;

class CmPayment extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'broker_agent_id', 'cm_contract_id', 'sequence', 'amount', 'payment_due_date', 'next_payment_due_date', 'date_paid', 'green_card', 'sticker', 'amount_without2', 'amount_no_sticker', 'created_at', 'created_by', 'updated_at', 'updated_by', 'deleted_by',
    ];

    protected $dates = ['deleted_at'];

    public function cm_contract()
    {
    	return $this->belongsTo('App\Models\CmContract');
    }

    public function user_created()
    {
        return $this->belongsTo('App\Models\User', 'created_by');
    }
}
