<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Auth;

class CmCustomer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'broker_agent_id', 'type', 'first_name', 'middle_name', 'last_name', 'egn_eik',
        'phone', 'phone2', 'created_at', 'created_by', 'updated_at', 'updated_by', 'deleted_at', 'deleted_by',
    ];

    public static function get_agent_customers()
    {
    	return self::where('created_by', Auth::id());
    }
}
