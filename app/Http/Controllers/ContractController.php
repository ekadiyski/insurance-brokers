<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ClProduct;
use App\Models\ClInsuranceCompany;
use App\Models\CmCustomer;
use App\Models\CmVehicle;
use App\Models\CmContract;
use App\Models\CmPayment;
use App\Models\User;

use Auth;

class ContractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cm_contracts = CmContract::where('created_by', Auth::id())->get();//TODO:filter

        return view('contracts.index', [
            'cm_contracts' => $cm_contracts
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cl_products = ClProduct::all();
        $cl_insurance_companies = ClInsuranceCompany::orderBy('broker_agent_id', 'desc')->get();
        $agent_customers = CmCustomer::get_agent_customers()->get();
        $agent_vehicles = CmVehicle::get_agent_vehicles()->get();

        return view('contracts.create', [
            'cl_products' => $cl_products,
            'cl_insurance_companies' => $cl_insurance_companies,
            'agent_customers' => $agent_customers,
            'agent_vehicles' => $agent_vehicles,
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'sometimes|required',
            'egn_eik' => 'sometimes|required',
            'existing_customer_id' => 'required_if:use_existing_customer,1',
            'cl_product_id' => 'required',
            'cl_insurance_company_id' => 'required',
            'politsa_number' => 'required',
            'payments_count' => 'required',
            'date_signed' => 'required',
            'payment' => 'required',
            'total_price' => 'required|numeric|min:14',
        ]);

        if($request->existing_customer_id && $request->use_existing_customer == 1){
            $cm_customer_id = $request->existing_customer_id;
        }else{
            $cm_customer = new CmCustomer();
            $cm_customer->first_name = $request->first_name;
            $cm_customer->broker_agent_id = $request->broker_agent_id;
            $cm_customer->middle_name = $request->middle_name;
            $cm_customer->last_name = $request->last_name;
            $cm_customer->egn_eik = $request->egn_eik;
            $cm_customer->phone = $request->phone;
            $cm_customer->phone2 = $request->phone2;
            $cm_customer->created_by = $request->created_by;
            $cm_customer->save();

            $cm_customer_id = $cm_customer->id;
        } 

        if($request->existing_customer_id){
            $cm_vehicle_id = $request->existing_vehicle_id;
        }elseif($request->name){
            $cm_vehicle = new CmVehicle();
            $cm_vehicle->broker_agent_id = $request->broker_agent_id;
            $cm_vehicle->name = $request->name;
            $cm_vehicle->brand = $request->brand;
            $cm_vehicle->talon_number = $request->talon_number;
            $cm_vehicle->owner_id = $cm_customer_id;
            $cm_vehicle->created_by = $request->created_by;
            $cm_vehicle->save();

            $cm_vehicle_id = $cm_vehicle->id;
        }else{
            $cm_vehicle_id = null;
        }

        $cm_contract = new CmContract();
        $cm_contract->broker_agent_id = $request->broker_agent_id;
        $cm_contract->cl_product_id = $request->cl_product_id;
        $cm_contract->cl_insurance_company_id = $request->cl_insurance_company_id;
        $cm_contract->politsa_number = $request->politsa_number;
        $cm_contract->date_signed = $request->date_signed;
        $cm_contract->start_date = $request->start_date;
        $cm_contract->cm_customer_id = $cm_customer_id;
        $cm_contract->cm_vehicle_id = $cm_vehicle_id;
        $cm_contract->payments_count = $request->payments_count;
        $cm_contract->left_payments_count = $request->left_payments_count;
        $cm_contract->total_price = $request->total_price;
        $cm_contract->already_paid = $request->payment[0];
        $cm_contract->created_by = $request->created_by;
        $cm_contract->save();

        $left_payments_count = $request->left_payments_count;
        for($i = 0; $i< $left_payments_count; $i++){
            $num = $i+1 + ($request->payments_count - $request->left_payments_count);
            $cm_payment = new CmPayment();
            $cm_payment->cm_contract_id = $cm_contract->id;
            $cm_payment->sequence = $num;
            $cm_payment->amount = $request->payment[$i];
            $cm_payment->amount_without2 = $request->amount_without2[$i];
            $cm_payment->amount_no_sticker = $request->amount_no_sticker[$i];
            $cm_payment->created_by = $request->created_by;
            $cm_payment->next_payment_due_date = $request->next_payment_due_date[$i];

            if($i > 0){
                $cm_payment->payment_due_date = $request->payment_due_date[$i];
            }else{
                $cm_payment->date_paid = $request->date_signed;//TODO: date signed or current date?
                if($request->green_card){
                    $cm_payment->green_card = $request->green_card;    
                }
                if($request->sticker){
                    $cm_payment->sticker = $request->sticker;    
                }
            }

            $cm_payment->save();
        }

        $politsa_number = $cm_contract->politsa_number;

        return redirect("/contracts")
            ->with('msg_add_success', "Полица $politsa_number беше добавена успешно!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cm_contract = CmContract::findOrFail($id);
        $cm_customer = CmCustomer::find($cm_contract->cm_customer_id)->first();
        $cm_vehicle = null;

        if($cm_contract->cm_vehicle_id){
            $cm_vehicle = CmVehicle::find($cm_contract->cm_vehicle_id)->first();
        }

        $cl_products = ClProduct::all();
        $cl_insurance_companies = ClInsuranceCompany::orderBy('broker_agent_id', 'desc')->get();
        $agent_customers = CmCustomer::get_agent_customers()->get();
        $agent_vehicles = CmVehicle::get_agent_vehicles()->get();

        return view('contracts.edit', [
            'cl_products' => $cl_products,
            'cl_insurance_companies' => $cl_insurance_companies,
            'agent_customers' => $agent_customers,
            'agent_vehicles' => $agent_vehicles,

            'cm_contract' => $cm_contract,
            'cm_customer' => $cm_customer,
            'cm_vehicle' => $cm_vehicle,
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'sometimes|required',
            'egn_eik' => 'sometimes|required',
            'existing_customer_id' => 'required_if:use_existing_customer,1',
            'cl_product_id' => 'required',
            'cl_insurance_company_id' => 'required'
        ]);
        $cm_contract = CmContract::findOrFail($id);

        if($request->existing_customer_id && $request->use_existing_customer == 1){
            $cm_customer_id = $request->existing_customer_id;
        }else{
            $cm_customer_id = $cm_contract->cm_customer_id;
            $cm_customer = CmCustomer::find($cm_contract->cm_customer_id);

            $cm_customer->first_name = $request->first_name;
            $cm_customer->broker_agent_id = $request->broker_agent_id;
            $cm_customer->middle_name = $request->middle_name;
            $cm_customer->last_name = $request->last_name;
            $cm_customer->egn_eik = $request->egn_eik;
            $cm_customer->phone = $request->phone;
            $cm_customer->phone2 = $request->phone2;
            $cm_customer->updated_by = Auth::id();

            $cm_customer->save();
        } 

        if($request->use_existing_vehicle){
            $cm_vehicle_id = $request->existing_vehicle_id;
        }elseif($request->name){
            $cm_vehicle_id = $cm_contract->cm_vehicle_id;

            $cm_vehicle =  CmVehicle::find($cm_contract->cm_vehicle_id)->first();
            $cm_vehicle->broker_agent_id = $request->broker_agent_id;
            $cm_vehicle->name = $request->name;
            $cm_vehicle->brand = $request->brand;
            $cm_vehicle->talon_number = $request->talon_number;
            $cm_vehicle->owner_id = $cm_customer_id;
            $cm_vehicle->updated_by = Auth::id();
            $cm_vehicle->save();
        }else{
            $cm_vehicle_id = null;
        }

        $cm_contract->broker_agent_id = $request->broker_agent_id;
        $cm_contract->cl_product_id = $request->cl_product_id;
        $cm_contract->cl_insurance_company_id = $request->cl_insurance_company_id;
        $cm_contract->cm_customer_id = $cm_customer_id;
        $cm_contract->cm_vehicle_id = $cm_vehicle_id;
        $cm_contract->updated_by = Auth::id();
        $cm_contract->save();

        $politsa_number = $cm_contract->politsa_number;

        return redirect("/contracts")
            ->with('msg_add_success', "Полица $politsa_number беше обновена успешно!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cm_contract = CmContract::findOrFail($id);
        $contract_payments = CmPayment::where('cm_contract_id', $id)->get();
        $politsa_number = $cm_contract->politsa_number;

        $cm_contract->delete();
        $cm_contract->deleted_by = Auth::id();
        $cm_contract->save();
        foreach($contract_payments as $payment){
            $payment->delete();
            $payment->deleted_by = Auth::id();
            $payment->save();
        }
        return redirect("/contracts")
            ->with('msg_delete', "Полица $politsa_number и плащаннията към нея бяха изтрити успешно!");   
    }
}
