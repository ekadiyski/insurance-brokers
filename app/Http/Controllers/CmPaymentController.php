<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CmContract;
use App\Models\CmPayment;

use Auth;

class CmPaymentController extends Controller
{

    public function add_payment($contract_id)
    {
        $cm_contract = CmContract::findOrFail($contract_id);
        $payments_history = CmPayment::where('cm_contract_id', $contract_id)->whereNotNull('date_paid')->get();
        $left_payments = CmPayment::where('cm_contract_id', $contract_id)->whereNull('date_paid')->get();
        $first_unpaid = CmPayment::where('cm_contract_id', $contract_id)->whereNull('date_paid')->orderBy('sequence', 'asc')->first();
        //dd($payments_history);
        return view('payments.add_payment', [
            'cm_contract' => $cm_contract,
            'payments_history' => $payments_history,
            'left_payments' => $left_payments,
            'first_unpaid' => $first_unpaid,
            ]);
    }

    public function store_payment(Request $request)
    {
        $this->validate($request, [
            'date_paid' => 'required'
        ]);

        $cm_payment = CmPayment::findOrFail($request->payment_id);
        $cm_payment->updated_by = $request->created_by;

        $cm_payment->date_paid = $request->date_paid;//TODO: date signed or current date?
        if($request->green_card){
            $cm_payment->green_card = $request->green_card;
        }
        if($request->sticker){
            $cm_payment->sticker = $request->sticker;    
        }
        
        $cm_payment->save();

        $paid_amount = $request->current_payment;

        $cm_contract = CmContract::findOrFail($cm_payment->cm_contract_id);
        $cm_contract->already_paid = $cm_contract->already_paid + $paid_amount;
        $cm_contract->save();
        $politsa_number = $cm_contract->politsa_number;

        return redirect("/payments")
            ->with('msg_add_success', "Успешно извършихте плащане на стойност $paid_amount лв. по полица $politsa_number!");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payments = CmPayment::with(['user_created', 'cm_contract', 'cm_contract.cm_customer', 'cm_contract.cm_vehicle'])
            ->where('created_by', Auth::id())
            ->whereNotNull('date_paid')
            ->get();

        return view('payments.index', [
            'payments' => $payments,
            ]);
    }

    public function company_payments()
    {
        $payments = CmPayment::with(['user_created', 'cm_contract', 'cm_contract.cm_customer', 'cm_contract.cm_vehicle'])
            ->where('broker_agent_id', 1)
            ->whereNotNull('date_paid')
            ->get();
        //dd($payments);

        return view('payments.index', [
            'payments' => $payments,
            ]);
    }

    /**
     * Display a listing of upcoming payments.
     *
     * @return \Illuminate\Http\Response
     */
    public function upcoming_payments()
    {
        $payments = CmPayment::with(['cm_contract', 'cm_contract.cm_customer', 'cm_contract.cm_vehicle'])
            ->where('created_by', Auth::id())
            ->whereNull('date_paid')
            ->get();

        return view('payments.upcoming_payments', [
            'payments' => $payments,
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
