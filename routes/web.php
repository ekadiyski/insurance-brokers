<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::middleware(['auth'])->group(function () {
	Route::resource('/contracts', 'ContractController');

	Route::get('/payments/upcoming', 'CmPaymentController@upcoming_payments');
	Route::get('/payments/add/{contract_id}', 'CmPaymentController@add_payment');
	Route::post('/payments/store', 'CmPaymentController@store_payment');
	Route::get('/payments/company', 'CmPaymentController@company_payments');
	Route::resource('/payments', 'CmPaymentController');

	Route::resource('/users', 'UserController');
});
